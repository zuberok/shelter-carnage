﻿using UnityEngine;
using System.Collections;
using StartApp;

public class StartAppOnEnableAD : MonoBehaviour {

	int skipAD = 0;

	void OnEnable()
	{
		skipAD++;
		if (skipAD == 3)
		{
			StartAppWrapper.showAd ();
			skipAD = 0;
		}
	}

	void OnDisable()
	{
		if (skipAD == 2)
			StartAppWrapper.loadAd ();
	}
}
