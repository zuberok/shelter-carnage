﻿using UnityEngine;
using System.Collections;

public class AnimationPlayLoading : MonoBehaviour {
	public GameObject [] small;
	public GameObject [] big;
	// Use this for initialization
	void Start () {
		StartCoroutine("Anim");

	}
	
	IEnumerator Anim()
	{
		while (true) 
		{
			for( int i=0; i<small.Length; i++ )
			{
				NGUITools.SetActive(small[i],false);
				NGUITools.SetActive(big[i],true);
				yield return new WaitForSeconds(0.3f);

			}
			for( int i=0; i<small.Length; i++ )
			{
				NGUITools.SetActive(small[i],true);
				NGUITools.SetActive(big[i],false);
				yield return new WaitForSeconds(0.3f);
			}
		}
		
	}
}
