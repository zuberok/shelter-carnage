﻿using UnityEngine;
using System.Collections;

public class DestroyMe : MonoBehaviour {
	public GameObject dirt;
	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForSeconds (0.2f);
		Instantiate (dirt, transform.position, transform.rotation);
		Destroy (gameObject);
	}
	

}
