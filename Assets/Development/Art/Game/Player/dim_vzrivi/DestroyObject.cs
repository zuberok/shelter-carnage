﻿using UnityEngine;
using System.Collections;

public class DestroyObject : MonoBehaviour {

	public float time;

	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForSeconds (time);
		Destroy (gameObject);

	}

}
