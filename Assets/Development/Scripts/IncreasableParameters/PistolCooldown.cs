﻿using UnityEngine;
using System.Collections;

public class PistolCooldown : MonoBehaviour {

	public UILabel priceLabel;
	static UILabel PriceLabel;
	public int[] prices;
	public static int[] Prices;
	public float[] values;
	static float[] Values;
	static UISlider slider;
	public static float CurrentValue
	{
		get{
			return Values[PlayerPrefs.GetInt("PISTOL_COOLDOWN_LEVEL")];}
	}
	public static int Level
	{
		set	
		{
			if(value == PlayerPrefs.GetInt("PISTOL_COOLDOWN_LEVEL"))
			{
				slider.value = (float)value / (Values.Length - 1); 
				try
				{
					PriceLabel.text = Prices[value+1].ToString();
				}
				catch
				{
					PriceLabel.text = "X";
				}
			}
			else
			{
				if(value < Values.Length && Money.Count >= Prices[value])
				{
					Money.Count -= Prices[value];
					slider.value = (float)value / (Values.Length - 1);
					PlayerPrefs.SetInt("PISTOL_COOLDOWN_LEVEL", value);
					try
					{
						PriceLabel.text = Prices[value+1].ToString();
					}
					catch
					{
						PriceLabel.text = "X";
					}
				}
			}
		}
		get	{return	PlayerPrefs.GetInt("PISTOL_COOLDOWN_LEVEL"); }
	}
	void Awake()
	{
		PriceLabel= priceLabel;
		slider = GetComponent<UISlider>();
		Prices = prices;	
		Values = values;

	}
	void OnEnable()
	{
		Level = PlayerPrefs.GetInt("PISTOL_COOLDOWN_LEVEL");
		Prices = prices;
		
	}
}
