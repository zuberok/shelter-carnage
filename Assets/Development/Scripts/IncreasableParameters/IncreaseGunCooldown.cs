﻿using UnityEngine;
using System.Collections;

public class IncreaseGunCooldown : MonoBehaviour {

	void OnClick()
	{
		if(PlayerPrefs.HasKey("GUN_AVAIABLE"))
		{
			GunCooldown.Level++;
		}
	}
}
