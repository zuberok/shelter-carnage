﻿using UnityEngine;
using System.Collections;

public class IncreaseRocketCooldown : MonoBehaviour {

	void OnClick()
	{
		if(PlayerPrefs.HasKey("BAZOOKA_AVAIABLE"))
		{
			RocketCooldown.Level++;
		}
	}
}
