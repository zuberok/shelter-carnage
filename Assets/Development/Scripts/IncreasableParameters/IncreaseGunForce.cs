﻿using UnityEngine;
using System.Collections;

public class IncreaseGunForce : MonoBehaviour {

	void OnClick()
	{
		if(PlayerPrefs.HasKey("GUN_AVAIABLE"))
		{
			GunForce.Level++;
		}
	}
}
