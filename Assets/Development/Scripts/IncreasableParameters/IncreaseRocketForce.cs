﻿using UnityEngine;
using System.Collections;

public class IncreaseRocketForce : MonoBehaviour {

	void OnClick()
	{
		if(PlayerPrefs.HasKey("BAZOOKA_AVAIABLE"))
		{
			RocketForce.Level++;
		}
	}
}
