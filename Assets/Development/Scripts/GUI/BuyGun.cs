﻿using UnityEngine;
using System.Collections;

public class BuyGun : MonoBehaviour {
	void OnEnable()
	{
		if(PlayerPrefs.HasKey("GUN_AVAIABLE"))
		{
			active = false;
		}
	}
	void OnClick()
	{
		if(int.Parse(transform.Find("Price").GetComponent<UILabel>().text) <= Money.Count)
		{
			PlayerPrefs.SetInt("GUN_AVAIABLE", 1);
			Money.Count -= int.Parse(transform.Find("Price").GetComponent<UILabel>().text);
			active = false;
		}
	}
}