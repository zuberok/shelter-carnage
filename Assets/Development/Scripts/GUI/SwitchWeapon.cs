﻿using UnityEngine;
using System.Collections;

public class SwitchWeapon : MonoBehaviour {

	public int weaponNumber;
	void OnPress()
	{
		Debug.Log("CLICKED");
		if(!((weaponNumber == 1 && !PlayerPrefs.HasKey("GUN_AVAIABLE")) || (weaponNumber == 2 && !PlayerPrefs.HasKey("BAZOOKA_AVAIABLE"))))
		{
			Sound.SwitchWeapon();
			Striker.SwitchWeapon(weaponNumber);
		}
	}
}
