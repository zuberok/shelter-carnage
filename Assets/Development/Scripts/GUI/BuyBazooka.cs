﻿using UnityEngine;
using System.Collections;

public class BuyBazooka : MonoBehaviour {
	void OnEnable()
	{
		if(PlayerPrefs.HasKey("BAZOOKA_AVAIABLE"))
		{
			active = false;
		}
	}
	void OnClick()
	{
		if(int.Parse(transform.Find("Price").GetComponent<UILabel>().text) <= Money.Count)
		{
			PlayerPrefs.SetInt("BAZOOKA_AVAIABLE", 1);
			Money.Count -= int.Parse(transform.Find("Price").GetComponent<UILabel>().text);
			active = false;
		}
	}
}
