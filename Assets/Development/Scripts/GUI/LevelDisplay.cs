﻿using UnityEngine;
using System.Collections;

public class LevelDisplay : MonoBehaviour {
	public GameObject playButton;
	public GameObject soonButton;
	void OnEnable()
	{
		int currentLevel = PlayerPrefs.GetInt("LEVEL") + 1;
		int locationNumber = ( currentLevel / 5 ) + 1;
		int levelNumber = currentLevel % 5;
		if(levelNumber == 0)
		{
			locationNumber--;
			levelNumber = 5;
		}

		GetComponent<UILabel>().text = "LEVEL " + locationNumber + '-' + levelNumber;
		if((PlayerPrefs.GetInt("LEVEL") + 1 > GameController.Levels.Length))
		{
			soonButton.active = true;
			active = playButton.active = false;
			Debug.Log((PlayerPrefs.GetInt("LEVEL") + 1) + " " + GameController.Levels.Length);
		}
	}
}
