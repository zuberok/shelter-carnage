﻿using UnityEngine;
using System.Collections;

public class RemoveIfAvaiable : MonoBehaviour {
	public string name;
	void OnEnable()
	{
		if(PlayerPrefs.HasKey(name + "_AVAIABLE"))
		{
			active = false;
		}
	}
}
