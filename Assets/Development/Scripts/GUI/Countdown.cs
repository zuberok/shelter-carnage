﻿using UnityEngine;
using System.Collections;

public class Countdown : MonoBehaviour {

	// Use this for initialization
	void OnEnable () {
		
		Invoke("DecreaseValue", 1f);
	}
	void DecreaseValue()
	{
		GetComponent<UILabel>().text = (int.Parse(GetComponent<UILabel>().text) - 1).ToString();
		try{
		if(int.Parse(GetComponent<UILabel>().text) < 1)
		{
			GetComponent<UILabel>().active = false;
		}
		else
		{
			Invoke("DecreaseValue", 1f);
		}
		}catch{}
		
	}
}
