﻿using UnityEngine;
using System.Collections;

public class GUIController : MonoBehaviour {
	public GameObject status;
	public static GameObject Status;
	public GameObject[] pausePanels;
	public static GameObject[] PausePanels;
	public GameObject[] upgradePanels;
	public GameObject[] gamePanels;
	public GameObject[] cooldownSprites;
	public UISlider healthBar;
	public UISlider armorBar;
	public static UISlider HealthBar;
	public static UISlider ArmorBar;
	private static GameObject[] UpgradePanels;
	public static GameObject[] CooldownSprites;
	public static GameObject[] GamePanels;
	public static bool Paused;
	void Awake()
	{
		Status = status;
		HealthBar = healthBar;
		ArmorBar = armorBar;
		UpgradePanels = upgradePanels;
		PausePanels = pausePanels;
		GamePanels = gamePanels;
		CooldownSprites = cooldownSprites;
	}
	public static void Upgrade()
	{
		foreach(GameObject go in GamePanels)
		{
			go.active = false;
		}
		foreach(GameObject go in UpgradePanels)
		{
			go.active = true;
		}
	}
	public static void Play()
	{
		Status.active = false;
		foreach(GameObject go in UpgradePanels)
		{
			go.active = false;
		}
		foreach(GameObject go in GamePanels)
		{
			go.active = true;
		}
	}
	public static void Pause()
	{
		Status.GetComponent<UILabel>().active = false;
		Paused = true;
		foreach (GameObject go in PausePanels)
		{
			go.active = true;
		}
		foreach(GameObject go in GamePanels)
		{
			go.active = false;
		}
	}
	public static void WakeUp()
	{
		foreach (GameObject go in PausePanels)
		{
			go.active = false;
		}
		foreach(GameObject go in GamePanels)
		{
			go.active = true;
		}
		Paused = false;
	}
	public static void ReadyWeapon(int i)
	{
		CooldownSprites[i].active = false;
	}
	public static void UnreadyWeapon(int i)
	{
		CooldownSprites[i].active = true;
	}
}
