﻿using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour {

	public AudioSource[] Sources;
	static AudioSource[] sources;
	static AudioClip pistolShoot;
	static AudioClip avtomatShoot;
	static AudioClip raketnicaShoot;
	static AudioClip raketnicaVzriv;
	static AudioClip strikeDamage;
	static AudioClip bossSpawn;
	static AudioClip[] enemyDamage;
	static AudioClip enemyDie;
	static AudioClip helicopterSpawn;
	public AudioClip PistolShoot;
	public AudioClip AvtomatShoot;
	public AudioClip RaketnicaShoot;
	public AudioClip RaketnicaVzriv;
	public AudioClip StrikeDamage;
	public AudioClip BossSpawn;//
	public AudioClip[] EnemyDamage;
	public AudioClip EnemyDie;
	public AudioClip HelicopterSpawn;
	public AudioClip WeaponSwitch;
	static AudioClip weaponSwitch;
	
	void Awake()
	{
		sources = Sources;
		weaponSwitch = WeaponSwitch;
		pistolShoot = PistolShoot;
		avtomatShoot = AvtomatShoot;
		raketnicaShoot = RaketnicaShoot;
		raketnicaVzriv = RaketnicaVzriv;
		strikeDamage = StrikeDamage;
		bossSpawn = BossSpawn;
		enemyDamage = EnemyDamage;
		enemyDie = EnemyDie;
		helicopterSpawn = HelicopterSpawn;
	}
	public static void SwitchWeapon()
	{
		sources[2].clip = weaponSwitch;
		sources[2].Play();
	}
	public static void Helicopter()
	{
		sources[0].clip = helicopterSpawn;
		sources[0].Play();
	}
	public static void DieEnemy()
	{
		sources[0].clip = enemyDie;
		sources[0].Play();
	}
	public static void DamagePistol()
	{
		sources[0].clip = enemyDamage[0];
		sources[0].Play();
	}
	public static void DamageGun()
	{
		try{
		sources[0].clip = enemyDamage[1];
		sources[0].Play();}
		catch
		{
			Debug.Log(sources.Length + " " + enemyDamage.Length);
		}
	}
	public static void SpawnBoss()
	{
		sources[1].clip = bossSpawn;
		sources[1].Play();
	}
	public static void Pistol()
	{
		sources[2].clip = pistolShoot;
		sources[2].Play();
	}
	public static void Gun()
	{
		sources[2].clip = avtomatShoot;
		sources[2].Play();
	}
	public static void Bazooka()
	{
		sources[2].clip = raketnicaShoot;
		sources[2].Play();
	}
	public static void RocketBang()
	{
		sources[1].clip = raketnicaVzriv;
		sources[1].Play();
	}
	public static void Stop(int i)
	{
		sources[i].Stop();
	}
	public static void GetDamage()
	{
		sources[3].clip = strikeDamage;
		sources[3].Play();
	}
}
