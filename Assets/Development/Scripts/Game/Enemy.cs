﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public bool isBoss;
	public GameObject bulletPrefab; //стрельба
	public float strikePeriod;
	public float cooldownDelta;
	Transform shot;


	public int maxHealth; //жизни
	int currentHealth;
	public GameObject HealthDisplay;
	GameObject healthDisplay;
	public Vector3 healthBarDelta;

	public int reward;
	public GameObject moneyDisplay; //вознаграждение

	public float normalSpeed;
	public float slowTime; //скорость
	public float slowSpeed;
	public float currentSpeed;
	Vector3 runAwayPosition;

	public float stopDistance;
	private bool gotTarget = false;
	public Vector3 goalPosition;
	
	public GameObject dieBloodPrefab;
	public GameObject bloodPrefab; //страдания
	
	public bool rotateForDegrees;
	void Awake()
	{
		if(gameObject.ToString()[0] == 'B')
		{
			Sound.SpawnBoss();
		}
		try{
			GetComponent<tk2dSpriteAnimator>().Play();
		}
		catch
		{
			for(int i = 1; i < 7; ++i)
			{
				transform.Find(i.ToString()).GetComponent<tk2dSpriteAnimator>().Play();
			}
		}
		shot = transform.Find("Shot").transform;
		goalPosition = Vector3.zero;
		runAwayPosition = transform.position;
		currentSpeed = normalSpeed;
		if(transform.position.x < 0f)
		{
			Vector3 _temp = transform.localScale;
			_temp.x = -1;
			transform.localScale = _temp;
		}
		if(rotateForDegrees)
		{
			RotateByClockwork();
		}
		currentHealth = maxHealth;
		
	}
	void RotateByClockwork()
	{
		if(transform.position.x > 0)
		{
			if(transform.position.y > 0)
			{
				transform.eulerAngles += Vector3.forward * 30f;
			}
			else
			{
				transform.eulerAngles += Vector3.back * 30f;
			}
		}
		if(transform.position.x < 0)
		{
			if(transform.position.y > 0)
			{
				transform.eulerAngles += Vector3.back * 30f;
			}
			else
			{
				transform.eulerAngles += Vector3.forward * 30f;
			}
		}
	}
	void NormalizeSpeed()
	{
		currentSpeed = normalSpeed;
	}
	public void TakeDamage(int d)
	{
		if(healthDisplay == null && HealthDisplay != null)
		{
			healthDisplay = (GameObject)Instantiate(HealthDisplay, transform.position, Quaternion.identity);
			Debug.Log(healthDisplay.transform.localPosition);
			healthDisplay.transform.parent = GUIController.Status.transform.parent;
			healthDisplay.transform.localScale = Vector3.one;
		}
		else
		{
			Debug.Log(healthDisplay + " " + HealthDisplay);
		}
		currentSpeed = slowSpeed;
		CancelInvoke("NormalizeSpeed");
		Invoke("NormalizeSpeed", slowTime);
		currentHealth -= d;
		if(healthDisplay != null)
		{
			healthDisplay.GetComponent<UISlider>().value = (float)currentHealth / maxHealth;
		}		
		if(currentHealth < 1)
		{
			Die();
			Sound.DieEnemy();
		}
		else
		{
			Instantiate(bloodPrefab, transform.position, Quaternion.identity);
		}
	}
	void Update()
	{
		if((!Striker.Alive || !gotTarget) && Time.timeScale == 1)
		{
			Move();
		}
		if(healthDisplay != null )
		{
		    healthDisplay.transform.localPosition = transform.position * 100f + healthBarDelta;
		}
	}
	void Move()
	{
		
		transform.position = Vector3.MoveTowards(transform.position, goalPosition, currentSpeed * Time.deltaTime);
		if(Vector3.Magnitude(transform.position) <= stopDistance && Striker.Alive)
		{
			gotTarget = true;
			SetAttackAnimation();
			Strike();
		}
		else if(Vector3.Magnitude(transform.position) < 0.5f)
		{
			Striker.DiePicture();
			Invoke("RunAway", Striker.EatingTime);
		}
	}
	void SetAttackAnimation()
	{
		if(gameObject.ToString()[0] == 'M')
		{
			try
			{
				GetComponent<tk2dSpriteAnimator>().Stop();
				GetComponent<tk2dSpriteAnimator>().playAutomatically = false;
			}
			catch
			{
				for(int i = 1; i < 7; ++i)
				{
					transform.Find(i.ToString()).GetComponent<tk2dSpriteAnimator>().Stop();
					transform.Find(i.ToString()).GetComponent<tk2dSpriteAnimator>().playAutomatically = false;
				}
			}
		}
	}
	void RunAway()
	{
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
		{
			try{go.GetComponent<Enemy>().GoHome();}
			catch{}
		}
	}
	public void GoHome()
	{
		try{
			GetComponent<tk2dSprite>().FlipX = true;
		}
		catch
		{
			for(int i = 1; i < 7; ++i)
			{
				transform.Find(i.ToString()).GetComponent<tk2dSprite>().FlipX = true;
			}
		}
		gotTarget = true;
		goalPosition = runAwayPosition * 1.2f;
	}
	private void Die()
	{
		
		Money.Count += reward;
		if(dieBloodPrefab != null)
		{
			Instantiate(dieBloodPrefab, transform.position, Quaternion.identity);
		}
		GameObject.Find("GameController").GetComponent<GameController>().Scan();
		GameObject moneyD = ((GameObject)Instantiate(moneyDisplay, transform.position / 5.33f, Quaternion.identity));
		moneyD.GetComponent<UILabel>().text = "+" + reward.ToString() + "$";//1.8 1
		moneyD.transform.parent = GUIController.Status.transform.parent;
		moneyD.transform.localPosition = transform.position * 100f;
		moneyD.transform.localScale = Vector3.one;
		if(isBoss)
		{
			foreach (GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
			{
				try{
					if(go != this.gameObject){go.GetComponent<Enemy>().Die();}
					Level.numberOfCurrentWave = Level.Waves.Length;
					Destroy(Level.currentWave);
					Level.noMoreMonsters = true;
					GameObject.Find("GameController").GetComponent<GameController>().Scan();
				}
				catch{}
			}
		}
		Destroy(gameObject);
	}
	void OnDestroy(){
		Destroy(healthDisplay);
		}
	void Strike()
	{
		PlayStrikeAnimation();
		if(Striker.Alive && Time.timeScale != 0)
		{
			Instantiate(bulletPrefab, shot.transform.position, Quaternion.identity);
			Invoke("Strike", strikePeriod + Random.Range(-cooldownDelta, cooldownDelta));
		}
		else if(!Striker.Alive)
		{
			gotTarget = false;
		}
	}
	void PlayStrikeAnimation()
	{
		if(gameObject.ToString()[0] == 'M')
		{
			try
			{
				GetComponent<tk2dSpriteAnimator>().Play(gameObject.ToString().Substring(0, 9) + "_Attack");
			}
			catch
			{
				for(int i = 1; i < 7; ++i)
				{
					transform.Find(i.ToString()).GetComponent<tk2dSpriteAnimator>().Play(gameObject.ToString().Substring(0, 9) + "_Attack");
				}
			}
		}
	}
}
