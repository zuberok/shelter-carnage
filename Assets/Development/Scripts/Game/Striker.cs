﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Striker : MonoBehaviour {

	public static float pistolTimer;
	public static float gunTimer;
	public static float rocketTimer;
	public float eatingTime;
	public static float EatingTime;

	public static int CurrentHealth;
	public static int CurrentArmor;

	public UISlider healthBar;
	public UISlider armorBar;

	public static GameObject LastBullet;
	public static bool Alive = true;
	public static GameObject striker;
	bool turnedRight;
	bool finished;
	public int health
	{
		set{
			healthBar.value = (float)value / Health.MaxValue;
			CurrentHealth = value;
		}
		get{
			return CurrentHealth;
		}
	}
	public int armor
	{
		set{
			armorBar.value = (float)value / Armor.MaxValue;
			CurrentArmor = value;
		}
		get{
			return CurrentArmor;
		}
	}

	public GameObject[] weapons;
	private static GameObject[] Weapons;
	//static GameObject Player;

	float soundTimer = 0f;
	public void TakeDamage(int d)
	{
		{
			if(d > armor)
			{
				d-= armor;
				armor = 0;
				health -= d;
			}
			else
			{
				armor -=d;
			}
			if(soundTimer > 2f)
			{
				Sound.GetDamage();
				soundTimer = 0;
			}
			GUIController.ArmorBar.value = (float)armor / Armor.MaxValue;
			if(health < 1)
			{
				Die();
			}
		}
	}
	public static void SwitchWeapon(int i)
	{
		Destroy(LastBullet);
		if(Weapons != null && i < Weapons.Length)
		{
			foreach (GameObject w in Weapons)
			{
				w.active = false;
			}
			Weapons[i].active = true;
		}
	}
	void Die()
	{
		Music.Death();
		
		Destroy(Level.currentLevel);
		Alive = false;
		foreach(GameObject go in GUIController.GamePanels)
		{
			go.active = false;
		}
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
		{
			try{
				go.GetComponent<Enemy>().currentSpeed *= 2f;
			}
			catch{
			}
		}

		Invoke("ExitGame", 5f);
	}
	void ExitGame()
	{
		GameController.FinishLevel(false);
	}
	public static void DiePicture()
	{
		foreach (GameObject w in Weapons)
		{
			w.active = false;
		}
		striker.GetComponent<tk2dSprite>().SetSprite("mertvii");
	}
	public void WinAnimation(float time)
	{
		if(!finished){
			foreach (GameObject w in Weapons)
			{
				w.active = false;
			}
			finished = true;
			PlayerPrefs.SetInt("LEVEL", PlayerPrefs.GetInt("LEVEL") + 1);
			Debug.LogWarning("LEVEL INCREASED");
			GUIController.Status.GetComponent<UILabel>().active = true;
			GUIController.Status.GetComponent<UILabel>().text = "VICTORY";
			Music.Winner();
			striker.GetComponent<tk2dSpriteAnimator>().Play("Win");
			striker.GetComponent<Striker>().Finish(time);
		}
	}
	public void Finish(float time)
	{
		Invoke("GoToUpgrade", time);
	}
	public void GoToUpgrade()
	{
		GameController.FinishLevel(true);
	}
	void Awake()
	{
		
		striker = gameObject;
		EatingTime = eatingTime;
		//Player = gameObject;
		Weapons = new GameObject[weapons.Length];
		for(int i = 0; i < weapons.Length; ++i)
		{
			Weapons[i] = Instantiate(weapons[i]) as GameObject;
			Weapons[i].active = false;
			Weapons[i].transform.parent = transform;
			Weapons[i].transform.localScale = Vector3.one - 2 * Vector3.right;
		}
		Weapons[0].active = true;
	}
	public void Spawn()
	{
		finished = false;
		striker.GetComponent<tk2dSpriteAnimator>().Play("Idle");
		striker.GetComponent<tk2dSprite>().SetSprite("_1_");
		active = true;
		health = Health.MaxValue;
		Alive = true;
		armor = Armor.MaxValue;
		SwitchWeapon(0);
	}
	Vector3 _temp;
	void Update()
	{
		rocketTimer += Time.deltaTime;
		gunTimer += Time.deltaTime;
		pistolTimer += Time.deltaTime;

		GUIController.CooldownSprites[0].GetComponent<UISprite>().fillAmount = 1 - pistolTimer / PistolCooldown.CurrentValue;
		GUIController.CooldownSprites[1].GetComponent<UISprite>().fillAmount = 1 - gunTimer / GunCooldown.CurrentValue;
		GUIController.CooldownSprites[2].GetComponent<UISprite>().fillAmount = 1 - rocketTimer / RocketCooldown.CurrentValue;


		soundTimer += Time.deltaTime;
		if(Input.GetMouseButton(0) && Alive)
		{
			_temp = transform.localScale;
			if(turnedRight)
			{
				if(Input.mousePosition.x < Screen.width / 2)
				{
					GetComponent<tk2dSprite>().FlipX = true;
					turnedRight = false;
					_temp.x = -1;
				}
			}
			else
			{
				if(Input.mousePosition.x > Screen.width / 2)
				{
					GetComponent<tk2dSprite>().FlipX = true;
					turnedRight = true;		
					_temp.x = 1;
				}
			}
			transform.localScale = _temp;
		}
	}
}
