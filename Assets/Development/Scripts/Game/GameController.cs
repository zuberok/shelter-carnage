﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
	public static Vector3[] defaultVectors = new Vector3[]
	{
		new Vector3(10F, 6F, 0),
		new Vector3(-10F, 6F, 0),
		new Vector3(-10F, -6F, 0),
		new Vector3(10F, -6F, 0),
	};
	public Striker player;
	public static Striker Player;
	public static GameObject[] Levels;
	public GameObject[] levels;
	static GameObject currentLevel;
	public int startCash;
	public int[] backChangeLevels;
	public static int[] BackChangeLevels;
	public GameObject[] backs;
	static GameObject[] Backs;
	static GameObject currentBack;
	public GameObject helicopter;
	static GameObject Helicopter;
	public Vector3 strikerOnHelicopter;
	public static Vector3 StrikerOnHelicopter;
	void Awake()
	{
		StrikerOnHelicopter = strikerOnHelicopter;
		Helicopter = helicopter;
		Application.targetFrameRate = 30;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		Backs = backs;
		Player = player;
		Levels = levels;
		BackChangeLevels = backChangeLevels;
	}
	void Start()
	{
		if(!PlayerPrefs.HasKey("LEVEL"))
		{
			PlayerPrefs.SetInt("LEVEL", 0);
			PlayerPrefs.SetInt("MONEY_AMOUNT", startCash);
			GUIController.Upgrade();
			GUIController.Play();
			CallHelicopter(false);
			StartLevel();
			Pause();
			GUIController.PausePanels[1].active = false;
		}
		else
		{
			Music.Menu();
			GUIController.Upgrade();
		}
	}
	public static void CallHelicopter(bool take)
	{
		GameObject hel = (GameObject)Instantiate(Helicopter, new Vector3(-20,15,0), Quaternion.identity);
		if(take)
		{
			hel.GetComponent<Helicopter>().take = true;
		}
		else
		{
			Transform strk = Striker.striker.transform;
			strk.parent = hel.transform;
			strk.localPosition = StrikerOnHelicopter;
		}
		
	}
	public static void StartLevel()
	{
		try
		{
			Music.Game();
			for(int i = BackChangeLevels.Length - 1; i >= 0; --i)
			{
				if(PlayerPrefs.GetInt("LEVEL") >= BackChangeLevels[i])
				{
					currentBack = (GameObject)Instantiate(Backs[i], Vector3.zero, Quaternion.identity);
					break;
				}
			}

			currentLevel = (GameObject)Instantiate(Levels[PlayerPrefs.GetInt("LEVEL")], Vector3.zero, Quaternion.identity);
			GUIController.Play();
			Player.Spawn();
		}
		catch
		{
			Debug.LogError("No more levels avaiable");
		}
	}

	
	public static void Pause()
	{
		GUIController.Pause();
		Time.timeScale = 0;
	}
	public static void WakeUp()
	{
		GUIController.WakeUp();
		if(!Player.active)
		{
			Player.active = true;
			Player.Spawn();
		}
		if(Striker.LastBullet != null)
		{
			Destroy(Striker.LastBullet);
		}
		Time.timeScale = 1;
	}
	public static void FinishLevel(bool success)
	{
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
		{
			Destroy(go);
		}
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Prints"))
		{
			Destroy(go);
		}
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("EnemyBullet"))
		{
			Destroy(go);
		}
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Back"))
		{
			Destroy(go);
		}
		Destroy(currentBack);
		Destroy(currentLevel);
		Player.active = false;
		GUIController.Upgrade();
		Music.Menu();
		
	}
	public void Scan()
	{
		if(Level.noMoreMonsters)
		{
			Invoke("Check",0.5f);
		}
	}
	private void Check()
	{
		if(GameObject.FindGameObjectsWithTag("Enemy").Length == 0)
		{
			
			if(IndexOf(PlayerPrefs.GetInt("LEVEL") + 1, backChangeLevels) != -1)
			{
				Striker.striker.GetComponent<Striker>().WinAnimation(9f);
				Invoke("InviteHelicopter",3f);
			}
			else
			{
				Striker.striker.GetComponent<Striker>().WinAnimation(6f);
			}
		}
	}
	void InviteHelicopter()
	{
		CallHelicopter(true);
	}
	public static int IndexOf(int element, int[] array)
	{
		for(int i = 0; i < array.Length; ++i)
		{
			if(array[i] == element)
			{
				return i;
			}
		}
		return -1;
	}
}
