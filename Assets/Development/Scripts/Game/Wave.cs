﻿using UnityEngine;
using System.Collections;

public class Wave : MonoBehaviour {
	public float timeBeforeStart;
	public float timeBetweenMonsters;


	public GameObject[] monsters;
	public int numberOfMonsters;
	
	private int monstersCount = 0;
	
	void Awake()
	{	
		monstersCount = 0;
		Invoke("CreateMonster", (float)timeBeforeStart);		
	}
	void CreateMonster()
	{
		monstersCount++;
		GameObject _temp = monsters[UnityEngine.Random.Range(0, monsters.Length)];
		Instantiate(_temp, Level.MonsterInitPositions[UnityEngine.Random.Range(0, Level.MonsterInitPositions.Length)], _temp.transform.rotation);
		
		if(monstersCount == numberOfMonsters)
		{
			Level.WaveFinished = true;
		}
		else
		{
			Invoke("CreateMonster", timeBetweenMonsters);
		}
	}
	void OnDestroy()
	{
		CancelInvoke();
	}
}
