﻿using UnityEngine;
using System.Collections;

public class Helicopter : MonoBehaviour {
	public Vector3 stopPosition;
	Vector3 goalPosition;
	Transform striker;
	public bool take;
	void Awake()
	{
		Sound.Helicopter();
		goalPosition = stopPosition;
	}
	void Start()
	{
		striker = GameObject.Find("Striker").transform;
	}
	void FixedUpdate()
	{
		transform.position = Vector3.MoveTowards(transform.position, goalPosition, 0.1f);
		if(Vector3.Magnitude(transform.position - goalPosition) < 0.4f && goalPosition == stopPosition)
		{
			if(take)
			{
				striker.parent = transform;
				striker.localPosition = GameController.StrikerOnHelicopter;
				Invoke("DropStriker", 2.4f);
			}
			else
			{
				striker.parent = null;
				striker.position = Vector3.zero;
			}
			goalPosition = new Vector3(20f, 15, 0);
		}
		else if (Vector3.Magnitude(transform.position - goalPosition) < 0.4f)
		{
			DropStriker();
		}
	}
	void DropStriker()
	{
		striker.parent = null;
		striker.position = Vector3.zero;
		Destroy(gameObject);
		Sound.Stop(0);
	}
}
