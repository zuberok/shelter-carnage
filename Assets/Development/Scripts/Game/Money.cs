﻿using UnityEngine;
using System.Collections;

public class Money : MonoBehaviour {

	static UILabel display;
	void Awake()
	{
		display = GetComponent<UILabel>();
		display.text = Count.ToString();
		
		//Debug.Log(Count);
	}
	public static int Count
	{
		get
		{
			return PlayerPrefs.GetInt("MONEY_AMOUNT");
		}
		set
		{
			if (value >= 0)
			{
				PlayerPrefs.SetInt("MONEY_AMOUNT", value);
				try{
				display.text = value.ToString();
				}
				catch
				{
					Debug.LogWarning("NO DISPLAY");
				}
			}
			else
			{
				Debug.LogWarning("MONEY < 0");
			}
		}
	}
}
