﻿using UnityEngine;
using System.Collections;

public class PauseOnPlay : MonoBehaviour {

	void OnClick()
	{
		GameController.Pause();
	}
	public static bool Hover = false;
	void OnMouseEnter()
	{
		Hover = true;
	}
	void OnMouseExit()
	{
		Hover = false;
	}

}
