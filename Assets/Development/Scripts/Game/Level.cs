﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {
	public static Vector3[] MonsterInitPositions;
	public Vector3[] monsterInitPositions;

	public GameObject[] waves;
	public static GameObject[] Waves;
	public static bool WaveFinished
	{
		set
		{
			if(value)
			{
				NextWave();
			}
		}
	}

	public static Level currentLevel;

	public static GameObject currentWave;
	public static int numberOfCurrentWave;
	public static bool noMoreMonsters;
	void Awake()
	{
		noMoreMonsters = false;
		MonsterInitPositions = monsterInitPositions;
		numberOfCurrentWave = 0;
		Waves = waves;
		currentWave = (GameObject)Instantiate(Waves[0]);
		currentLevel = this;
		if(PlayerPrefs.GetInt("LEVEL") != PlayerPrefs.GetInt("HELICOPTER_CALLED") && GameController.IndexOf(PlayerPrefs.GetInt("LEVEL"), GameController.BackChangeLevels) != -1)
		{
			PlayerPrefs.SetInt("HELICOPTER_CALLED", PlayerPrefs.GetInt("LEVEL"));
			GameController.CallHelicopter(false);
		}
	}
	public static void NextWave()
	{
		Destroy(currentWave);
		WaveFinished = false;
		try
		{
			numberOfCurrentWave++;
			currentWave = (GameObject)Instantiate(Waves[numberOfCurrentWave]);
		}
		catch
		{
			Destroy(currentLevel.gameObject);
			noMoreMonsters = true;
		}
	}
	void OnDestroy()
	{
		Destroy(currentWave);
	}
}
