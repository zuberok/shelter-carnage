﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {

	public GameObject bullet;
	public  Transform shot;
	bool ready = true;
	TweenRotation tr;
	TweenPosition tp;
	void Awake()
	{
		tp = GetComponent<TweenPosition>();
		tr = GetComponent<TweenRotation>();
		ready = false;
		shot = transform.Find("Shot").transform;
	}
	void Strike(Vector3 dir)
	{
		if(Striker.Alive)
		{
		Striker.gunTimer = 0;
		Striker.LastBullet = (GameObject)Instantiate(bullet, shot.position, Quaternion.identity);
		Striker.LastBullet.GetComponent<GunBullet>().goalPosition = dir;
		ready = false;
		GUIController.UnreadyWeapon(1);
		tp.active = false;
		tr.ResetToBeginning ();
		tr.PlayForward();
		tp.active = true;}
	}
	void Update()
	{
		if(!ready)
		{
			if(Striker.gunTimer > GunCooldown.CurrentValue)
			{
				ready = true;
				GUIController.ReadyWeapon(1);
			}
		}
		if(Input.GetMouseButton(0) && ready)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit; 
	        Vector3 goalPosition = Vector3.zero;       
	        if (Physics.Raycast(ray, out hit, Mathf.Infinity)) 
	        {
	        	if(hit.collider.CompareTag("Back") || hit.collider.CompareTag("Enemy"))
	        	{
	            	goalPosition = hit.point;
	        	}
	        } 
	        if(goalPosition != Vector3.zero)
	        {
				Strike(goalPosition);
	        }
		}
	}
}
