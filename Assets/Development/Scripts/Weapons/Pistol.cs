﻿using UnityEngine;
using System.Collections;

public class Pistol : MonoBehaviour {

	public GameObject bullet;
	private Transform[] shots;
	bool ready = true;
	TweenRotation tr;
	TweenPosition tp;
	void Awake()
	{
		tp = GetComponent<TweenPosition>();
		tr = GetComponent<TweenRotation>();
		ready = false;
		shots = new Transform[2]{transform.Find("Shot1").transform, transform.Find("Shot2").transform};
	}
	void Start()
	{
		transform.localPosition = Vector3.zero;
	}
	void Strike(Vector3 dir)
	{
		if(Striker.Alive)
		{
			Striker.LastBullet = (GameObject)Instantiate(bullet, shots[UnityEngine.Random.Range(0, shots.Length)].position, Quaternion.identity);
			Striker.LastBullet.GetComponent<PistolBullet>().goalPosition = dir;
			ready = false;
			Striker.pistolTimer = 0;
			GUIController.UnreadyWeapon(0);
			tp.active = false;
			tr.ResetToBeginning ();
			tr.PlayForward();
			tp.active = true;
		}
	}
	void Update()
	{
		if(!ready)
		{
			if(Striker.pistolTimer > PistolCooldown.CurrentValue)
			{
				ready = true;
				GUIController.ReadyWeapon(0);
			}
		}
		if(Input.GetMouseButtonDown(0) && ready)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit; 
	        Vector3 goalPosition = Vector3.zero;       
	        if (Physics.Raycast(ray, out hit, Mathf.Infinity)) 
	        {
	        	if(hit.collider.CompareTag("Back") || hit.collider.CompareTag("Enemy"))
	        	{
	            	goalPosition = hit.point;
	        	}
	        } 
	        if(goalPosition != Vector3.zero)
	        {
				Strike(goalPosition);
	        }
	        Debug.Log(goalPosition);
		}
	}
}
