﻿using UnityEngine;
using System.Collections;

public class Bazooka : MonoBehaviour {

	public GameObject rocket;
	private Transform shot;
	bool ready;
	TweenRotation tr;
	TweenPosition tp;
	void Awake()
	{
		tp = GetComponent<TweenPosition>();
		tr = GetComponent<TweenRotation>();
		ready = true;
		shot = transform.Find("Shot").transform;

	}
	void Strike(Vector3 dir)
	{
		if(Striker.Alive && ready){
			Striker.rocketTimer = 0;
			Striker.LastBullet = (GameObject)Instantiate(rocket, shot.position, Quaternion.identity);
			Striker.LastBullet.GetComponent<Rocket>().goalPosition = dir;
			ready = false;
			GUIController.UnreadyWeapon(2);
			tp.active = false;
			tr.ResetToBeginning ();
			tr.PlayForward();
			tp.active = true;
		}
	}
	void Update()
	{
		if(!ready)
		{
			if(Striker.rocketTimer > RocketCooldown.CurrentValue)
			{
				ready = true;
				GUIController.ReadyWeapon(2);
			}
		}
		if(Input.GetMouseButtonDown(0) && ready)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit; 
	        Vector3 goalPosition = Vector3.zero;       
	        if (Physics.Raycast(ray, out hit, Mathf.Infinity)) 
	        {
	        	if(hit.collider.CompareTag("Back") || hit.collider.CompareTag("Enemy"))
	        	{
	            	goalPosition = hit.point;
	        	}
	        } 
	        if(goalPosition != Vector3.zero)
	        {
				Strike(goalPosition);
	        }
		}
	}
}
