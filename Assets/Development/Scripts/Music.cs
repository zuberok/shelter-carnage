﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {

	static AudioSource source;
	static AudioClip menuMusic;
	public AudioClip MenuMusic;
	static AudioClip gameMusic;
	public AudioClip GameMusic;
	static AudioClip deathMusic;
	public AudioClip DeathMusic;
	static AudioClip winnerMusic;
	public AudioClip WinnerMusic;

	void Awake()
	{
		source = GetComponent<AudioSource>();
		menuMusic = MenuMusic;
		gameMusic = GameMusic;
		deathMusic = DeathMusic;
		winnerMusic = WinnerMusic;
	}

	public static void Game()
	{
		source.clip = gameMusic;
		source.loop = true;
		source.Play();
	}

	public static void Menu()
	{
		source.clip = menuMusic;
		source.loop = true;
		source.Play();
	}

	public static void Death()
	{
		source.clip = deathMusic;
		source.loop = false;
		source.Play();
	}

	public static void Winner()
	{
		source.clip = winnerMusic;
		source.loop = false;
		source.Play();
	}
}
