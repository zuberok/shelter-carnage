﻿using UnityEngine;
using System.Collections;

public class RocketBang : MonoBehaviour {
	public float radius;
	static int damage;
	void Start()
	{
		damage = RocketForce.CurrentValue;
		foreach(Collider coll in Physics.OverlapSphere(transform.position, radius))
		{
			if(coll.CompareTag("Enemy"))
			{
				coll.GetComponent<Enemy>().TakeDamage(damage);
			}
			else if(coll.CompareTag("EnemyBullet"))
			{
				Debug.Log(coll.gameObject);
				Destroy(coll.gameObject);
			}
		}
	}
}
