﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PistolBullet : MonoBehaviour {

	static int damage;
	public float speed;
	public Vector3 moveVector;
	public Vector3 goalPosition;
	Vector3 goalRotation;
	public float rotationSpeed = 200f;
	void Start()
	{
		int quater = goalPosition.x > 0 ? goalPosition.y > 0 ? 1 : 4 :  goalPosition.y > 0 ? 2 : 3;
        if(GetNearestMonster(quater) != null)
		{
			moveVector = GetNearestMonster(quater).position - transform.position;
			Debug.LogWarning("MONSTERAUTOFIND QUATER: " + quater);
		}
		else
		{
			moveVector = GameController.defaultVectors[quater - 1];
			Debug.LogError("DEFAULT STRIKE VECTOR");
		}
		moveVector = Vector3.Normalize(moveVector);
		damage = PistolForce.CurrentValue;
		if(moveVector.y > 0)
		{
			goalRotation.z = (Mathf.Acos(moveVector.x) * Mathf.Rad2Deg + 270f) % 360f;
		}
		else
		{
			goalRotation.z = (270f - Mathf.Acos(moveVector.x) * Mathf.Rad2Deg) % 360f;
		}
		transform.eulerAngles = new Vector3(0, 0, Mathf.Sign(moveVector.x) * 90f + 180f);
		Sound.Pistol();
	}
	void Update()
	{
		transform.position += moveVector * Time.deltaTime * speed;
		transform.eulerAngles = Vector3.MoveTowards(transform.eulerAngles, goalRotation, rotationSpeed * Time.deltaTime);
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("Enemy"))
		{
			try{col.GetComponent<Enemy>().TakeDamage(damage);}
			catch{}
			Destroy(gameObject);
			Sound.DamageGun();
		}
		else if(col.CompareTag("EnemyBullet"))
		{
			Destroy(gameObject);
			Destroy(col.gameObject);
		}
	}
	void OnBecameInvisible()
	{
		Destroy(gameObject);
	}
	Transform GetNearestMonster(int quater)
	{
		//int quater = ;
		List<Transform> allInThisQuater = new List<Transform>();
		Transform result;
		
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
		{
			switch(quater)
			{
				case 1:
					if(go.transform.position.x > 0 && go.transform.position.y > 0)
						allInThisQuater.Add(go.transform);
					break;
				case 2:
					if(go.transform.position.x < 0 && go.transform.position.y > 0)
						allInThisQuater.Add(go.transform);
					break;
				case 3:
					if(go.transform.position.x < 0 && go.transform.position.y < 0)
						allInThisQuater.Add(go.transform);
					break;
				case 4:
					if(go.transform.position.x > 0 && go.transform.position.y < 0)
						allInThisQuater.Add(go.transform);
					break;
			}
		}
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("EnemyBullet"))
		{
			switch(quater)
			{
				case 1:
					if(go.transform.position.x > 0 && go.transform.position.y > 0)
						allInThisQuater.Add(go.transform);
					break;
				case 2:
					if(go.transform.position.x < 0 && go.transform.position.y > 0)
						allInThisQuater.Add(go.transform);
					break;
				case 3:
					if(go.transform.position.x < 0 && go.transform.position.y < 0)
						allInThisQuater.Add(go.transform);
					break;
				case 4:
					if(go.transform.position.x > 0 && go.transform.position.y < 0)
						allInThisQuater.Add(go.transform);
					break;
			}
		}
		if(allInThisQuater.Count == 0)
			{return null;}
		result = allInThisQuater[0];
		for (int i = 1; i < allInThisQuater.Count; ++i)
		{
			if( Vector3.Magnitude(allInThisQuater[i].position) < Vector3.Magnitude(result.position))
			{
				result = allInThisQuater[i];
			}
		}
		return result;
	}
}
