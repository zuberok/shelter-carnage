﻿using UnityEngine;
using System.Collections;

public class EnemyBullet : MonoBehaviour {

	public float speed;
	public int damage;
	void Awake()
	{
		Vector3 _temp = transform.eulerAngles;
		if(transform.position.y > 0)
		{
			_temp.z = Mathf.Acos(Vector3.Normalize(transform.position).x) * Mathf.Rad2Deg + 180f;
			if(transform.position.x > 0)
			{
				_temp.z += 180f;
			}
		}
		else
		{
			_temp.z = Mathf.Acos(Vector3.Normalize(transform.position).x) * Mathf.Rad2Deg - 90f;
		}
		if(transform.position.x < 0)
		{
			GetComponent<tk2dSprite>().FlipX = true;
		}
		transform.eulerAngles = _temp;
	}
	void Update () {
		transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, speed * Time.deltaTime);
	}
	void OnTriggerEnter(Collider col){
		if(col.CompareTag("Player"))
		{
			col.GetComponent<Striker>().TakeDamage(damage);
			Destroy(gameObject);
		}
	}
}
